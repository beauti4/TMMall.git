#
# Be sure to run `pod lib lint z02sckhd_6wqgx_mall.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'z02sckhd_6wqgx_mall'
  s.version          = '1.0.0'
  s.summary          = 'TMMall framework component'

  s.description      = <<-DESC
  天马商城组件打包上传，增加集成支付
                       DESC

  s.homepage         = 'https://gitee.com/beauti4/TMMall'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'OYangHui' => '442371047@qq.com' }
  s.source           = { :git => 'https://gitee.com/beauti4/TMMall.git', :tag => s.version.to_s }

  s.swift_version = '4.2'
  s.ios.deployment_target = '9.0'
  s.requires_arc = true
  s.xcconfig = {
  'VALID_ARCHS' => 'arm64 x86_64',
  }

  s.source_files = 'z02sckhd_6wqgx_user/Classes/z02sckhd_6wqgx_user.framework/Headers/*.{h}'
  s.resource  = 'z02sckhd_6wqgx_user/Classes/z02sckhd_6wqgx_user.bundle'
  s.vendored_frameworks = 'z02sckhd_6wqgx_user/Classes/z02sckhd_6wqgx_user.framework'

  s.dependency 'Kingfisher'
  s.dependency 'Cosmos'
  s.dependency 'YHTool', '~> 1.1.6'

  s.dependency 'MJRefresh', '~> 3.1.12'

  s.dependency 'TMUserCenter'
  s.dependency 'TMPaySDK'



end
